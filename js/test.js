(function() {
  const myQuestions = [
    {
      question: "PACK: WOLVES. SCHOOL: ?",
      answers: {
        a: "Flowers",
        b: "Flies",
        c: "Fish",
        d: "Lions"
      },
      correctAnswer: "c"
    },
    {
      question: "Which of the following is not an opposite of GARRULOUS?",
      answers: {
        a: "Taciturn",
        b: "Concise",
        c: "Loquacious",
        d: "Reticent"
      },
      correctAnswer: "c"
    },
    {
      question: "A person who spends money in an extravagant, irresponsible way is (a):",
      answers: {
        a: "Frugal",
        b: "Thrifty",
        c: "Spendthrift",
        d: "Conservative"
      },
      correctAnswer: "c"
    },
    {
      question: "If 5 persons can execute 15 items in a checklist in 10 days, how many days will 15 persons take to execute 5 items in the checklist?",
      answers: {
        a: "Between 3 & 4 days",
        b: "Between 1 & 2 days",
        c: "Between 5 & 6 days",
        d: "Greater than 5 days"
      },
      correctAnswer: "b"
    },
    {
      question: "What is the next number in the sequence? 11, 44, 99, ?",
      answers: {
        a: 111,
        b: 176,
        c: 133,
        d: 113 
      },
      correctAnswer: "b"
    },
    {
      question: "Identify the missing number in the sequence: 29, 31, ? , 41",
      answers: {
        a: 33,
        b: 36,
        c: 35,
        d: 37 
      },
      correctAnswer: "d"
    },
    {
      question: "Statements: A lullaby is a song. No song is prose. Some proses are epics. Conclusions: I. Some proses are songs. II. Some epics are lullabies. III. Some songs are lullabies.",
      answers: {
        a: "Neither of I, II or III follows",
        b: "Only III follows",
        c: "Only II follows",
        d: "Only I follows"
      },
      correctAnswer: "b"
    },
    {
      question: "Statements: No public school charges fees. Some public schools are residential. Conclusions: I. Private schools charge fees. II. Some residential schools charge fees",
      answers: {
        a: "Only conclusion I follows",
        b: "Both I and II follow",
        c: "Neither I nor II follows",
        d: "Only conclusion II follows"
      },
      correctAnswer: "c"
    }
    
  ];

  function buildQuiz() {
    const output = [];

    myQuestions.forEach((currentQuestion, questionNumber) => {
      const answers = [];

      // and for each available answer...
      for (letter in currentQuestion.answers) {
        // ...add an HTML radio button
        answers.push(
          `<label>
             <input type="radio" name="question${questionNumber}" value="${letter}">
              ${letter} :
              ${currentQuestion.answers[letter]}
           </label>`
        );
      }

      // add this question and its answers to the output
      output.push(
        `<div class="slide">
           <div class="question"> ${currentQuestion.question} </div>
           <div class="answers"> ${answers.join("")} </div>
         </div>`
      );
    });

    // finally combine our output list into one string of HTML and put it on the page
    quizContainer.innerHTML = output.join("");
  }

  

  function showResults() {
    // gather answer containers from our quiz
    const answerContainers = quizContainer.querySelectorAll(".answers");

    let mark = 10;
    // keep track of user's answers
    let numCorrect = 0;
    let totalCorrect = numCorrect * mark;

    // for each question...
    myQuestions.forEach((currentQuestion, questionNumber) => {
      // find selected answer
      const answerContainer = answerContainers[questionNumber];
      const selector = `input[name=question${questionNumber}]:checked`;
      const userAnswer = (answerContainer.querySelector(selector) || {}).value;

      // if answer is correct
      if (userAnswer === currentQuestion.correctAnswer) {
        // add to the number of correct answers
        numCorrect++;
        totalCorrect = numCorrect * mark;
      } else {
      //  answerContainer[questionNumber].style.color = "red";
      }
    });

    // show number of correct answers out of total
    document.querySelector("#quiz").style.display = "none";
    document.querySelector(".buttons").style.display = "none";
    
    if(totalCorrect >= 60) {
      document.getElementById("results").innerHTML = `Congratulations! You scored ${numCorrect} out of ${myQuestions.length} questions. We'll get in touch.`;
    }
    else{
      document.getElementById("results").innerHTML = `You scored ${numCorrect} out of ${myQuestions.length} questions. Sorry, we can't take you further.`;
    }
    
  }

  function showSlide(n) {
    slides[currentSlide].classList.remove("active-slide");
    slides[n].classList.add("active-slide");
    currentSlide = n;
    
    if (currentSlide === 0) {
      previousButton.style.display = "none";
    } else {
      previousButton.style.display = "inline-block";
    }
    
    if (currentSlide === slides.length - 1) {
      nextButton.style.display = "none";
      submitButton.style.display = "inline-block";
    } else {
      nextButton.style.display = "inline-block";
      submitButton.style.display = "none";
    }
  }

  function showNextSlide() {
    showSlide(currentSlide + 1);
  }

  function showPreviousSlide() {
    showSlide(currentSlide - 1);
  }

  const quizContainer = document.getElementById("quiz");
  const resultsContainer = document.getElementById("results");
  const submitButton = document.getElementById("submit");

  // display quiz right away
  buildQuiz();

  const previousButton = document.getElementById("previous");
  const nextButton = document.getElementById("next");
  const slides = document.querySelectorAll(".slide");
  let currentSlide = 0;

  showSlide(0);

  // on submit, show results
  submitButton.addEventListener("click", showResults);
  previousButton.addEventListener("click", showPreviousSlide);
  nextButton.addEventListener("click", showNextSlide);
})();


// Get the modal login button
var modal = document.getElementById('modal-button');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

